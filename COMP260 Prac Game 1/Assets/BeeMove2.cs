﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove2 : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
		
	public float speed = 4.0f;        // metres per second
	public float turnSpeed = 180.0f;  // degrees per second
	public Transform target, target2;
	public Vector2 heading = Vector3.right; 

	void Update () {

		// to calculate closest player, create a vector for the distance between
		// each player
		Vector2 p1Direction = target.position - transform.position;
		Vector2 p2Direction = target2.position - transform.position;

		// now we compare which distance is smaller and set our direction vector's value accordingly
		Vector2 direction;
		if (p1Direction.magnitude > p2Direction.magnitude)
			direction = p1Direction;
		else
			direction = p2Direction;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}

		transform.Translate(heading * speed * Time.deltaTime);
	}

	void OnDrawGizmos() {
		// draw heading vector in red
		Gizmos.color = Color.red;
		Gizmos.DrawRay(transform.position, heading);

		// draw target vector in yellow
		Gizmos.color = Color.yellow;
		Vector2 direction = target.position - transform.position;
		Gizmos.DrawRay(transform.position, direction);
	}

}
