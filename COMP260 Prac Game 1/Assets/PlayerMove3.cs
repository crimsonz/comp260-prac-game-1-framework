﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMove3 : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}

	public Vector2 velocity; // in metres per second
	public Vector2 direction; // get the input values
	public float maxSpeed = 5.0f; // in metres per second
	public float brake = 5.0f; // in metres/second/second
	public float acceleration = 1.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float turnSpeed = 30.0f; // in degrees/second
	public string Vertical;
	public string Horizontal;

	void Update () {

		if (Vertical == ("WS"))
		{
			direction.y = Input.GetAxis("WS");
		}
		else if (Vertical == ("UD"))
		{
			direction.y = Input.GetAxis("UD");	
		}

		if (Horizontal == ("AD"))
		{
			direction.x = Input.GetAxis("AD");
		}
		else if (Horizontal == ("LR"))
		{
			direction.x = Input.GetAxis("LR");
		}

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("UD");
		// the horizontal axis controls the turn
		float turn = -Input.GetAxis("LR");
	
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}
		else {
			// braking
			float brakeRate = brake * Time.deltaTime;

			if (speed > brakeRate){
				speed -= brakeRate;
			} else if (speed < -brakeRate){
				speed += brakeRate;
			} else {
				speed = 0;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
		//turn the car
		transform.Rotate(0, 0, (turn * turnSpeed * Time.deltaTime) / (speed / maxSpeed));
		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed; 
		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}