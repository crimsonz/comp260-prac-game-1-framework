﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerMove2 : MonoBehaviour {
	// Use this for initialization
	void Start () {
	}
		
	public Vector2 velocity; // in metres per second
	public Vector2 direction; // get the input values
	public float maxSpeed = 5.0f;
	public string Vertical;
	public string Horizontal;

	void Update () {
		
		if (Vertical == ("WS"))
		{
			direction.y = Input.GetAxis("WS");
		}
		else if (Vertical == ("UD"))
		{
			direction.y = Input.GetAxis("UD");	
		}

		if (Horizontal == ("AD"))
		{
			direction.x = Input.GetAxis("AD");
		}
		else if (Horizontal == ("LR"))
		{
			direction.x = Input.GetAxis("LR");
		}

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;
		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}
