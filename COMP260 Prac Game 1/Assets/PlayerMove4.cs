﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public float maxSpeed = 5.0f;
	public float acceleration = 10.0f; //m/s/s
	public float brake = 5.0f; // m/s/s
	private float speed = 0.0f;
	public float turnSpeed = 30.0f; // in degrees per second
	public float destroyRadius = 1.0f;

	public string horizontalAxis = "Horizontal";
	public string verticalAxis = "Vertical";

	private BeeSpawner beeSpawner;

	// Use this for initialization
	void Start () {
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees (
				transform.position, destroyRadius);
		}

		// horizontal axis controls the turn
		float turn = -Input.GetAxis("Horizontal");//making it negative fixes inverted rotation bug

		// vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");

		if (forwards > 0) {
			// accelerated forwards
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		} else {
			// braking
			float brakeRate = brake * Time.deltaTime;//fixes the braking bug

			if (speed > brakeRate) {
				speed = speed - brakeRate;
			} else if (speed < -brakeRate) {
				speed = speed + brakeRate;
			} else {
				speed = 0;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// turn the car proportionally to the speed
		transform.Rotate(0, 0, (turn * turnSpeed * Time.deltaTime) / (speed / maxSpeed));

		//Compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		//move the boject
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}
}
